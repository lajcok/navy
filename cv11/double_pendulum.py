import tkinter as tk

import numpy as np
from scipy.integrate import odeint


def pendulum_motion(g=9.80665):
    """
    Pendulum Motion derivative functor
    :param g: Custom gravity acceleration constant
    :return: Returns derivative function
    """

    def _pend(theta, _, l1, l2, m1, m2):
        """
        Derivative for double pendulum
        :param theta: Quadruple in shape `(theta1position, theta2position, theta1speed, theta2speed)`
        :param _: Time specifier (unused)
        :param l1: Wire 1 length
        :param l2: Wire 2 length
        :param m1: Pendulum 1 mass
        :param m2: Pendulum 2 mass
        :return: Returns derivatives in shape `(theta1speed, theta2speed, theta1acceleration, theta2acceleration)`
        """
        thp1, thp2, ths1, ths2 = theta  # unpack theta values
        th_diff = thp1 - thp2
        sin_th, cos_th = np.sin(th_diff), np.cos(th_diff)

        tha1 = (
                       m2 * g * np.sin(thp2) * cos_th
                       - m2 * sin_th * (l1 * ths1 ** 2 * cos_th + l2 * ths2 ** 2)
                       - (m1 + m2) * g * np.sin(thp1)
               ) / (l1 * (m1 + m2 * sin_th ** 2))
        tha2 = (
                       (m1 + m2) * (l1 * ths1 ** 2 * sin_th - g * np.sin(thp2) + g * np.sin(thp1) * cos_th)
                       + m2 * l2 * ths2 ** 2 * sin_th * cos_th
               ) / (l2 * (m1 + m2 * sin_th ** 2))

        return ths1, ths2, tha1, tha2

    return _pend


class DoublePendulum:
    """
    Double Pendulum wrapper class
    Holds some pendulum properties
    """

    def __init__(self, l1, l2, m1, m2, theta_position, theta_speed=None, t=10, g=9.80665):
        """
        Constructor initializing double pendulum
        :param l1: Wire 1 length
        :param l2: Wire 2 length
        :param m1: Pendulum 1 mass
        :param m2: Pendulum 2 mass
        :param theta_position: Initial angles
        :param theta_speed: Initial speed, defaults to `(0,0)`
        :param t: Number of seconds to simulate
        :param g: Custom gravity acceleration constant
        """
        self.l1, self.l2 = l1, l2
        self.m1, self.m2 = m1, m2
        self.theta_speed = theta_speed if theta_speed is not None else (0, 0)
        self.t = np.linspace(0, t, t * 120)
        self.theta_position = theta_position
        self.g = g
        # compute motion
        self.motion = odeint(
            pendulum_motion(g),
            y0=(*self.theta_position, *self.theta_speed),
            t=self.t,
            args=(l1, l2, m1, m2),
        )

    def __iter__(self):
        """
        Iterate through motion
        :return: Returns iterator yielding tuple of theta angle positions `(theta1, theta2)`
        """
        i = 0
        while True:
            i %= self.t.size
            yield self.motion[i][:2]
            i += 1


class DoublePendulumGUI:
    """
    Double Pendulum GUI Class
    """

    wire_hang = .5  # relative hang position in canvas
    len_unit = 200  # wire length factor
    mass_unit = 20  # pendulum mass factor

    def __init__(self, master, pendulum, size=800):
        """
        Initiate GUI
        :param master: Tkinter root
        :param pendulum: Instance of `DoublePendulum` class with desired properties
        :param size: Canvas will be `size x size`
        """
        self.master = master
        self.pendulum = pendulum
        self.size = size

        self.canvas = tk.Canvas(master, width=size, height=size)
        self.canvas.pack()

        # background
        self.canvas.create_rectangle(0, 0, self.size, self.size, fill='white', outline='')
        # hang point
        self.canvas.create_oval(
            *(self.wire_hang * size + 2 * np.repeat((-1, +1), 2)),
            fill='black',
        )
        self.wire1 = self.canvas.create_line(0, 0, 0, 0)
        self.wire2 = self.canvas.create_line(0, 0, 0, 0)
        self.pendulum1 = self.canvas.create_oval(0, 0, 0, 0, fill='red', outline='')
        self.pendulum2 = self.canvas.create_oval(0, 0, 0, 0, fill='red', outline='')

        # store iterator and update canvas
        self.pendulum_iterator = iter(self.pendulum)
        self.update()

    def update(self):
        """
        Iterates pendulum motion and renders changes
        """
        # obtain angles, lengths, masses
        th1, th2 = next(self.pendulum_iterator)
        l1, l2 = self.pendulum.l1 * self.len_unit, self.pendulum.l2 * self.len_unit
        m1, m2 = self.pendulum.m1 * self.mass_unit, self.pendulum.m2 * self.mass_unit

        # compute hang point and pendulum locations
        xy0 = np.broadcast_to(self.wire_hang * self.size, shape=(2,))
        xy1 = xy0 + l1 * np.array((np.sin(th1), np.cos(th1)))
        xy2 = xy1 + l2 * np.array((np.sin(th2), np.cos(th2)))

        # update wires
        self.canvas.coords(self.wire1, *xy0, *xy1)
        self.canvas.coords(self.wire2, *xy1, *xy2)

        # update pendulums
        self.canvas.coords(self.pendulum1, *(np.tile(xy1, 2) + m1 * np.repeat((-1, +1), 2)))
        self.canvas.coords(self.pendulum2, *(np.tile(xy2, 2) + m2 * np.repeat((-1, +1), 2)))

        # compute FPS based on given time range and set timer
        fps = self.pendulum.t.size / self.pendulum.t[-1]
        self.master.after(int(1000 / fps), self.update)


def main():
    # pendulum properties
    pendulum = DoublePendulum(
        l1=.7, l2=.8,
        m1=.4, m2=.5,
        theta_position=(np.pi / 3, 5 * np.pi / 8),
        t=30
    )

    # motion visualization
    root = tk.Tk()
    DoublePendulumGUI(root, pendulum)
    root.mainloop()


if __name__ == '__main__':
    main()
