import sys
import time
import tkinter as tk

import numpy as np


def substitute(axiom, rules):
    """
    Substitutes variables in axiom according to rules
    :param axiom: Axiom string
    :param rules: Rules to apply to axiom
    :return: Returns new axiom
    """
    pattern = axiom
    yield pattern
    while True:
        pattern = ''.join([
            rules[c] if c in rules else c
            for c in pattern
        ])
        yield pattern


class Canvas:
    """
    Represents canvas for given L-System
    """
    def __init__(self, master, size=800, padding=20):
        """
        Initiates canvas
        :param master: Tk() root
        :param size: Size of the canvas used for x and y
        :param padding: Padding to add to canvas
        """
        self.size = size
        self.padding = padding
        self.canvas = tk.Canvas(master, width=size + 2 * padding, height=size + 2 * padding)
        self.canvas.pack()
        self.clear()

    def clear(self):
        """
        Clears the canvas white
        """
        self.canvas.create_rectangle(
            0, 0,
            self.size + 2 * self.padding, self.size + 2 * self.padding,
            fill='white'
        )

    def render(self, pattern, angle):
        """
        Renders given pattern to the canvas
        :param pattern: Axiom string
        :param angle: Angle to use for painting
        """
        self.clear()

        # init: position = [0,0], orientation = 0 rad
        position = np.zeros(2, dtype=float)
        orientation = 0.

        # collect lines to draw with step 1
        stack = []      # LIFO stack
        lines = []      # lines to make storage
        for c in pattern:
            if c == 'F':
                new_position = position + np.array([
                    np.cos(orientation),    # x shift
                    np.sin(orientation),    # y shift
                ])
                lines.append((position, new_position))      # save line for later painting
                position = new_position
            elif c == 'b':
                position += np.array([
                    np.cos(orientation),    # x shift
                    np.sin(orientation),    # y shift
                ])
            elif c == '+':
                orientation -= angle
            elif c == '-':
                orientation += angle
            elif c == '[':
                stack.append((position, orientation))
            elif c == ']':
                position, orientation = stack.pop()

        # if no lines produced, skip painting
        if len(lines) <= 0:
            return

        # convert to matrix
        lines_matrix = np.array([
            [x0, y0, x1, y1]
            for (x0, y0), (x1, y1) in lines
        ])

        # normalize to [0;1] interval
        mn, mx = np.min(lines_matrix), np.max(lines_matrix)     # normalizes all dimensions proportionally
        rng = mx - mn
        norm_matrix = (lines_matrix - mn) / rng if (rng > 0).all() else lines_matrix - mn

        # spread on the canvas full size by scaling, add padding offset
        spread_matrix = norm_matrix * self.size + self.padding

        # paint
        for line in spread_matrix:
            self.canvas.create_line(*line, fill='black')


if __name__ == '__main__':
    # Implemented L-Systems, extensible
    systems = [
        # 1
        {
            'axiom': 'F+F+F+F',
            'rules': {'F': 'F+F-F-FF+F+F-F'},
            'angle': np.pi / 2,
        },
        # 2
        {
            'axiom': 'F++F++F',
            'rules': {'F': 'F+F--F+F'},
            'angle': np.pi / 3,
        },
        # 3
        {
            'axiom': 'F',
            'rules': {'F': 'F[+F]F[-F]F'},
            'angle': np.pi / 7,
        },
        # 4
        {
            'axiom': 'F',
            'rules': {'F': 'FF+[+F-F-F]-[-F+F+F]'},
            'angle': np.pi / 8,
        },
        # via https://fedimser.github.io/l-systems.html
        # 5 A bush
        {
            'axiom': 'Y',
            'rules': {
                'X': 'X[-FFF][+FFF]FX',
                'Y': 'YFX[+Y][-Y]',
            },
            'angle': np.pi / 7,
        },
    ]
    # 1-based order of the L-System to use as an argument or a default
    choice = int(sys.argv[1]) if len(sys.argv) > 1 else 4

    root = tk.Tk()
    canvas = Canvas(root)
    iterations = 5   # stop after n iterations
    # delay = .5  # delay between re-painting

    _axiom, _rules, _angle = systems[choice - 1].values()

    frame = None
    for i, frame in zip(range(iterations), substitute(_axiom, _rules)):
        print(i, frame)
        # canvas.render(frame, _angle)
        # root.update_idletasks()
        # time.sleep(delay)

    canvas.render(frame, _angle)
    root.mainloop()
