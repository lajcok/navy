import sys

import numpy as np
from matplotlib import pyplot as plt, animation
from mpl_toolkits.mplot3d import Axes3D  # noqa: F401 unused import


def ifs(transformations, x=np.zeros(3), p=None):
    """
    Iterated function system for fractals
    :param transformations: 2D matrix of shape (t, 12) where `t` is number of possible transformations
    :param x: Initial position `x` in 3D, defaults to [0,0,0]
    :param p: Probability vector specifying distribution of transformations, defaults to `None` => fair
    :return: Returns an iterator
    """
    t_options = np.arange(transformations.shape[0])
    _x = np.array(x)
    yield _x
    while True:
        # choose random transformation
        t_choice = np.random.choice(t_options, p=p)
        t = transformations[t_choice]
        # compute new x
        _x = t[:9].reshape(3, 3).dot(_x) + t[9:]
        yield _x


models = [
    # 1st model
    np.array([
        [0.00, 0.00, 0.01, 0.00, 0.26, 0.00, 0.00, 0.00, 0.05, 0.00, 0.00, 0.00, ],
        [0.20, -0.26, -0.01, 0.23, 0.22, -0.07, 0.07, 0.00, 0.24, 0.00, 0.80, 0.00],
        [-0.25, 0.28, 0.01, 0.26, 0.24, -0.07, 0.07, 0.00, 0.24, 0.00, 0.22, 0.00],
        [0.85, 0.04, -0.01, -0.04, 0.85, 0.09, 0.00, 0.08, 0.84, 0.00, 0.80, 0.00],
    ]),
    # 2nd model
    np.array([
        [0.05, 0.00, 0.00, 0.00, 0.60, 0.00, 0.00, 0.00, 0.05, 0.00, 0.00, 0.00],
        [0.45, -0.22, 0.22, 0.22, 0.45, 0.22, -0.22, 0.22, -0.45, 0.00, 1.00, 0.00],
        [-0.45, 0.22, -0.22, 0.22, 0.45, 0.22, 0.22, -0.22, 0.45, 0.00, 1.25, 0.00],
        [0.49, -0.08, 0.08, 0.08, 0.49, 0.08, 0.08, -0.08, 0.49, 0.00, 2.00, 0.00],
    ]),
]

if __name__ == '__main__':
    # select model via arguments, defaults to 2
    m = int(sys.argv[1]) + 1 if len(sys.argv) > 1 else 2
    model = models[m - 1]

    # visualization
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1, projection='3d')

    ax.set_xlabel('X')
    ax.set_ylabel('Y')
    ax.set_zlabel('Z')

    ax.set_xlim(-1, +4)
    ax.set_ylim(-1, +4)
    ax.set_zlim(-1, +2)

    # initial position plot
    init_x = np.zeros(3)
    res = np.array([init_x])
    scatter_plot, = ax.plot(*res.T, linestyle='', marker='.', c='k', markersize=1)

    # animation setup
    def update(arg):
        global res
        _, xs = arg
        res = np.append(res, xs.reshape(1, 3), axis=0)
        scatter_plot.set_data(*res.T[:2])
        scatter_plot.set_3d_properties(res.T[2])

    ani = animation.FuncAnimation(
        fig,
        update,
        frames=enumerate(ifs(model, x=init_x)),
        interval=1,
        blit=False,     # disabled for better in-plot rotation experience
    )
    plt.show()
