import numpy as np

from matplotlib import animation


def fractal_landscape(perturbation=20., roughness=2., raise_prob=.6, water_level=0., init_vertex_matrix=None):
    """
    Iterator-based implementation of Fractal Landscape generator
    In each iteration, a new array is being allocated - not optimized for great numbers of iteration
    :param perturbation: This set the overall height of the landscape.
    :param roughness: This is normally the factor by which the perturbations are reduced on each iteration.
            A factor of 2 is the usual default, lower values result in a rougher terrain,
            higher values result in a smoother surface.
    :param raise_prob: Probability to for the ground to be raised
    :param water_level: Bottom limit, cannot go any deeper
    :param init_vertex_matrix: Optional specification of initial landscape to begin with
    :return: Yields the vertex matrix in every iteration
    """
    vertex_matrix = init_vertex_matrix.copy() if init_vertex_matrix is not None else np.zeros((2, 2))
    while True:
        yield vertex_matrix

        # increase array size
        new_shape = np.array(vertex_matrix.shape) * 2 - 1
        new_vm = np.empty(new_shape)
        new_vm[::2, ::2] = vertex_matrix

        # fill the new inter-vertices linearly (using mean)
        new_vm[1::2, ::2] = (vertex_matrix[1:] + vertex_matrix[:-1]) / 2    # vertically, column-wise
        new_vm[:, 1::2] = (new_vm[:, :-1:2] + new_vm[:, 2::2]) / 2          # horizontal gaps

        # perturb
        grid_mask = np.ones_like(new_vm, dtype=bool)    # mask specifying where to perform perturbation
        grid_mask[::2, ::2] = False                     # eliminate original matrix vertices
        new_vm[grid_mask] += perturbation * (raise_prob - np.random.random_sample(grid_mask.sum()))

        # water level constraint
        new_vm[new_vm < water_level] = water_level

        # iterate
        vertex_matrix = new_vm
        perturbation /= roughness


if __name__ == '__main__':
    np.random.seed(0)   # seed-able

    _max_iterations = 12
    _perturbation = 20.
    _raise_probability = .6
    _fl = fractal_landscape(
        perturbation=_perturbation,
        roughness=2.3,
        raise_prob=_raise_probability,
        water_level=10.,
    )

    import matplotlib.pyplot as plt
    from mpl_toolkits.mplot3d import Axes3D
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    _zlf = 2.5
    _z_lim = _zlf * (_raise_probability - 1) * _perturbation, _zlf * _raise_probability * _perturbation

    def update(args):
        _i, _vertex_matrix = args
        print(_i, _vertex_matrix.shape)

        x, y = np.meshgrid(*[np.linspace(0, 1, dim) for dim in _vertex_matrix.shape])
        ax.clear()
        ax.set_zlim(*_z_lim)
        ax.plot_surface(x, y, _vertex_matrix, cmap='terrain')

    ani = animation.FuncAnimation(
        fig, update,
        frames=zip(range(_max_iterations), _fl),
        interval=100,
        repeat=False,
        blit=False,
    )

    plt.show()
