import numpy as np

from cv03.hopfield import Hopfield


def preview_image(image):
    return ''.join([
        ''.join([
            '\u2588\u2588' if cell else '\u2591\u2591'
            for cell in row
        ]) + '\n'
        for row in image
    ])


if __name__ == '__main__':
    print('original patterns:\n')
    patterns = list(map(np.array, [
        [
            # 8
            [0, 1, 1, 1, 0],
            [0, 1, 0, 1, 0],
            [0, 1, 1, 1, 0],
            [0, 1, 0, 1, 0],
            [0, 1, 1, 1, 0],
        ], [
            # 2
            [0, 1, 1, 1, 0],
            [0, 0, 0, 1, 0],
            [0, 1, 1, 1, 0],
            [0, 1, 0, 0, 0],
            [0, 1, 1, 1, 0],
        ], [
            # X
            [1, 0, 0, 0, 1],
            [0, 1, 0, 1, 0],
            [0, 0, 1, 0, 0],
            [0, 1, 0, 1, 0],
            [1, 0, 0, 0, 1],
        ]
    ]))
    h = Hopfield()
    for img in patterns:
        h.save(img)
        print(preview_image(img))

    print('distorted images:\n')
    avg_distort = 3
    distorted = [img.copy() for img in patterns]
    for img in distorted:
        n = np.round(np.random.normal(avg_distort)).astype(int)
        i = np.random.randint(img.size, size=n)
        idx = np.unravel_index(i, img.shape)

        img[idx] = ~img[idx].astype(bool)

        print(preview_image(img))

    print('synchronous recovery:\n')
    sync_recovery = [h.recover_sync(img) for img in patterns]
    for img in sync_recovery:
        print(preview_image(img))

    print('asynchronous recovery:\n')
    async_recovery = [h.recover_async(img) for img in patterns]
    for img in async_recovery:
        print(preview_image(img))
