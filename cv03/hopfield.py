import numpy as np


class Hopfield:
    def __init__(self, image=None, w=0):
        self.W = np.array(w)
        if image is not None:
            self.W = self.__class__(w=self.W).save(image).W

    def save(self, image):
        flat = np.array(image).flatten().reshape((1, -1))
        flat[flat == 0] = -1

        w = np.dot(flat.T, flat)
        np.fill_diagonal(w, 0)

        self.W = self.W + w

    def recover_sync(self, image):
        image = np.array(image)
        flat = image.flatten().reshape((1, -1))
        flat[flat == 0] = -1

        rec = self.W.dot(flat.T)

        rec[rec < 0] = 0
        return np.sign(rec.reshape(image.shape))

    def recover_async(self, image):
        image = np.array(image)
        flat = image.flatten()
        flat[flat == 0] = -1

        for i in range(1, flat.shape[-1]):
            flat[i] = np.sign(flat.dot(self.W[:, [i]]))

        flat[flat < 0] = 0
        return flat.reshape(image.shape)

