import numpy as np


def sigmoid(x):
    return 1 / (1 + np.exp(-x))


def sigmoid_derivative(x):
    return np.exp(-x) / (1 + np.exp(-x)) ** 2


class Neuron:
    def __init__(self, ws=np.zeros(1), eta=.1, fn=sigmoid):
        self.ws = np.array(ws)
        self.eta = eta
        self.fn = fn

    def _input(self, xs):
        pt = np.ones_like(self.ws)
        pt[:len(xs)] = xs
        return pt

    def net(self, xs):
        return np.dot(self.ws, self._input(xs))

    def out(self, xs):
        return self.fn(self.net(xs))

    def teach(self, delta_error_ws):
        ws = self.ws - self.eta * delta_error_ws
        return self.__class__(
            ws=ws,
            eta=self.eta,
            fn=self.fn,
        )
