import numpy as np

from cv02.network import Neuron, sigmoid_derivative

if __name__ == '__main__':
    h1, h2, o = [Neuron(np.random.uniform(size=3)) for _ in range(3)]
    pts = np.stack(np.meshgrid(*[np.arange(2)] * 2), axis=-1).reshape(-1, 2)
    res = np.bitwise_xor(*pts.T)


    def net(neurons, xs):
        h1, h2, o = neurons
        oo = o.out([h1.out(xs), h2.out(xs)])
        return oo


    def teach(neurons, xs, target):
        h1, h2, o = neurons

        net_h1, net_h2 = h1.net(xs), h2.net(xs)
        out_h1, out_h2 = h1.out(xs), h2.out(xs)

        pt_h = np.array([out_h1, out_h2, 1])
        net_o = o.net(pt_h)
        out_o = o.out(pt_h)

        d_err_out_o = out_o - target
        d_out_net_o = sigmoid_derivative(net_o)
        d_net_o_w56b = np.array([out_h1, out_h2, 1])

        d_net_o_out_h1, d_net_o_out_h2, _ = o.ws

        d_out_net_h1 = sigmoid_derivative(net_h1)
        d_out_net_h2 = sigmoid_derivative(net_h2)

        d_net_h1_ws13b, d_net_h2_ws24b = [np.array([*xs, 1])] * 2

        d_err_ws56b = d_err_out_o * d_out_net_o * d_net_o_w56b
        d_err_ws13b = d_err_out_o * d_out_net_o * d_net_o_out_h1 * d_out_net_h1 * d_net_h1_ws13b
        d_err_ws24b = d_err_out_o * d_out_net_o * d_net_o_out_h2 * d_out_net_h2 * d_net_h2_ws24b

        return h1.teach(d_err_ws13b), h2.teach(d_err_ws24b), o.teach(d_err_ws56b)


    _h1, _h2, _o = h1, h2, o
    epochs = int(10e3)
    for i in range(epochs + 1):
        guesses = np.array([net((_h1, _h2, _o), pt) for pt in pts])
        errors = np.abs(guesses - res)
        if i % 1000 == 0:
            print(i, 'guesses:', guesses, 'errors:', errors, 'error mean:', errors.mean())
        for pt, r in zip(pts, res):
            _h1, _h2, _o = teach((_h1, _h2, _o), pt, r)
