import time

import gym
import numpy as np
from keras import Sequential
from keras.layers import Dense
from keras.optimizers import Adam

env = gym.make('CartPole-v0')


def sample_frames():
    """
    Creates samples for one episode
    :return: Returns triplet (list of observations, list of actions, total reward)
    """
    observations = []
    actions = []
    total_reward = 0.

    observation = env.reset()
    while True:
        action = env.action_space.sample()

        observations.append(observation)
        actions.append(action)

        observation, reward, done, _ = env.step(action)
        total_reward += reward

        if done:
            break

    return observations, actions, total_reward


def train_cases(min_reward=50.):
    """
    A generator for training samples data set, filters out cases of rewards less then min_reward
    :param min_reward: Threshold - a minimum
    :return: Yields tuples (observation, action) - as many as necessary
    """
    while True:
        observations, actions, reward = sample_frames()
        if reward >= min_reward:
            yield from zip(observations, actions)


class CartPoleNN:
    """
    Class representing neural network used to predict pole balance
    """

    def __init__(self, learning_rate=.001):
        """
        Initiates Keras NN model
        :param learning_rate: Learning rate to use
        """
        self.model = Sequential()
        self.model.add(Dense(24, input_dim=4, activation='relu'))
        self.model.add(Dense(24, activation='relu'))
        self.model.add(Dense(2, activation='linear'))
        self.model.compile(loss='mse', optimizer=Adam(lr=learning_rate))

    def train(self, observation, action):
        """
        Pass data to train the model
        :param observation: Observation, 4D array
        :param action: An index of action
        """
        target = np.zeros(2)
        target[action] = 1
        # TODO consider training in grater chunks
        self.model.fit(
            x=observation.reshape(1, 4),
            y=target.reshape(1, 2),
            verbose=0,
        )

    def predict(self, observation):
        """
        Uses NN model to make prediction based on observation
        :param observation: Observation to use
        :return: Returns suggested action
        """
        predication = self.model.predict(observation.reshape(1, 4))
        return np.argmax(predication)


if __name__ == '__main__':
    # Init
    nn = CartPoleNN()

    # Training
    n_samples = 1000
    for i, (obs, act) in zip(range(n_samples), train_cases()):
        # print(i, obs, act)
        nn.train(obs, act)

    # Testing
    n_episodes = 10     # episodes to test
    sleep_time = .01    # slows down animation
    for i in range(n_episodes):
        obs = env.reset()   # initial observation
        env.render()
        time.sleep(sleep_time)

        score = 0.
        _done = False
        while not _done:
            act = nn.predict(obs)   # obtain predicted action
            obs, _reward, _done, _ = env.step(act)  # pass to cart-pole
            score += _reward

            env.render()
            time.sleep(sleep_time)

        print(f'Ep#{i} score: {score}')

    env.close()

# TEST CASES
# Ep#0 score: 159.0; frames: 159
# Ep#1 score: 173.0; frames: 173
# Ep#2 score: 163.0; frames: 163
# Ep#3 score: 200.0; frames: 200
# Ep#4 score: 200.0; frames: 200
# Ep#5 score: 185.0; frames: 185
# Ep#6 score: 157.0; frames: 157
# Ep#7 score: 193.0; frames: 193
# Ep#8 score: 169.0; frames: 169
# Ep#9 score: 200.0; frames: 200
