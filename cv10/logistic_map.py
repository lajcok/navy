from functools import reduce

import numpy as np


def logistic_map(r, x):
    """
    Logistic map implementation
    :param r: Parameter `r`, aka `a`
    :param x: Initial `x` values
    :return: Yields `x[n]` value on `n`th step
    """
    yield x * np.ones_like(r)
    while True:
        x = r * x * (1 - x)
        yield x


def animate(r, xns, predicted, actual, iter_max):
    """
    Animates logistic map iteration using given parameters
    :param r: Parameter `r`, aka `a`
    :param xns: 2D array of shape `(iter_max, r.size)`
                containing `x[n]` values for each iteration
    :param predicted: Tuple `(r, xp)` of `r`s and predicted values `xp` for them
    :param actual: Tuple `(r, x[n])` of `r`s and values `x[n]` after `n` iterations for them
    :param iter_max: Iterations limit
    """
    from matplotlib import pyplot as plt
    fig = plt.figure('Bifurcation animation')

    ax = fig.add_subplot(111)
    ax.set_xlabel('r')
    ax.set_ylabel('x[n]')
    ax.set_ylim(0, 1)

    # # static final bifurcation diagram
    # ax.scatter(r, xns[-1], marker='.', c='k', alpha=.4, s=1)
    ax.plot(*actual, label='Actual', c='red', linewidth=1)              # actual plot
    ax.plot(*predicted, label='Predicted', c='green', linewidth=1)      # prediction plot

    ax.legend()

    # bifurcation diagram construction animated
    from matplotlib import animation
    scat = ax.scatter(r, xns[0], marker='.', c='k', alpha=.4, s=1)      # scatter-plot

    def update(frame):
        n, xn = frame
        print(f'x[{n + 1}]')
        # ax.set_title(f'x[{n + 1}]')
        scat.set_offsets(np.vstack((r, xn)).T)
        return ax,

    ani = animation.FuncAnimation(
        fig, update,
        frames=enumerate(xns),
        interval=10,
        repeat=False,
        blit=True,
    )

    plt.show()


class LogisticMapNN:
    """
    Logistic map prediction neural network
    """
    def __init__(self, learning_rate=.001):
        """
        Initiates Keras NN model
        :param learning_rate: Learning rate to use
        """
        from keras import Sequential
        from keras.layers import Dense
        from keras.optimizers import Adam
        from keras import activations as a

        # NN model definition
        self.model = Sequential()
        self.model.add(Dense(2, input_dim=2, activation=a.tanh))
        self.model.add(Dense(20, activation=a.tanh))
        self.model.add(Dense(20, activation=a.tanh))
        self.model.add(Dense(1, activation=a.sigmoid))
        self.model.compile(loss='mse', optimizer=Adam(lr=learning_rate))

    def train(self, xs, ys):
        """
        Bulk train the network
        :param xs: 2D array of shape `(train_size, 2)` where each row represents one instance of input `(r, x)`
        :param ys: Results of shape `(train_size, 1)` with `x[n]` values after `n` iterations
        """
        self.model.fit(xs, ys)

    def predict(self, xs):
        """
        Get predictions for given inputs
        :param xs: 2D array of shape `(train_size, 2)` with each row used as an input for the NN
        :return: Returns prediction for each of input values
        """
        prediction = self.model.predict(xs)
        return prediction.flatten()


def main(iterations=50, samples=50, resolution=400):
    # initial values
    r = np.linspace(0, 4, resolution)       # `r` range
    x = np.random.random_sample(samples)    # `x` values for every `r`

    # match sizes to form tuples
    rs = np.repeat(r, x.size)
    xs = np.tile(x, r.size)

    # iterate
    xns = np.empty((iterations, xs.size))
    for i, xn in zip(range(iterations), logistic_map(rs, xs)):
        xns[i] = xn

    # NN
    nn = LogisticMapNN()
    # uses plot data to train
    nn.train(
        xs=np.vstack((rs, xs)).T,
        ys=xns[-1].reshape(-1, 1),
    )

    # prediction
    x_predict = np.broadcast_to(np.random.random_sample(), r.shape)
    prediction = nn.predict(np.vstack((r, x_predict)).T)

    # actual results for comparison
    actual = reduce(lambda a, b: b, (
        xn
        for _, xn in zip(
            range(iterations),
            logistic_map(r, x_predict)
        )
    ))

    # run animation
    animate(
        rs, xns,
        predicted=np.vstack((r, prediction)),
        actual=np.vstack((r, actual)),
        iter_max=iterations
    )


if __name__ == '__main__':
    main()
