import os
import time

import numpy as np
import tkinter as tk


class FindTheCheese:
    def __init__(self, board, actions=None, game_over=None, current_position=None):
        """
        Initialize the game
        :param board: 2D board specifying rewards
        :param actions: Supported actions, defaults to: down, right, up, left.
                        It is an 2D array of shape (n_of_actions, board.ndim)
                            - each row specifies offset in all dimensions
        :param game_over: Boolean array of the same shape as board, specifying field, in which the game ends,
                            defaults to fields with non-zero reward
        :param current_position: Initial / current position of the agent, defaults to y,x = [0,0]
        """
        self.board = np.array(board)

        self.position = np.zeros(self.board.ndim, dtype=int)
        if current_position is not None:
            self.position[:] = current_position

        if actions is None:
            mv = np.zeros([self.board.ndim] * 2, dtype=int)
            np.fill_diagonal(mv, 1)
            self.actions = np.vstack((mv, -mv))
        else:
            self.actions = np.array(actions, dtype=int)

        self.game_over = self.board != 0 if game_over is None else game_over

    def r_q_matrices(self):
        """
        Generates R, Q matrices as ndarrays of shape (n_of_states, n_of_actions) based on the board
            - n_of_states = board.size - each field
        :return: Return (R,Q) matrices
        """
        # pads the board around if NaN values, +1 row/col in each direction (4)
        padded = np.full(np.array(self.board.shape) + 2, np.nan)
        padded[1:-1, 1:-1] = self.board

        # Create views that reflect shifted board for each of the actions (down, right, up, left).
        # e.g. if I move from [0,0] up, I can access next state by accessing [0,0]
        # on shifted board to get NaN (boarder case), simply I will get the value of the next reward
        shifted = [
            padded[(1 + dy):(len(padded) - 1 + dy), (1 + dx):(len(padded) - 1 + dx)]
            for dy, dx in self.actions
        ]

        # I use that shifted board to generate R matrix of shape (board.size=25, n_of_actions=4)
        # each column contains rewards if agent moves in the specified direction from current state
        # R(current_state, action) = reward on the next field
        r_matrix = np.empty((self.board.size, self.actions.shape[0]))
        for i, states in enumerate(shifted):
            r_matrix[:, i] = states.flatten()

        # Q matrix is filled with zeros but walls are represented in it as NaNs
        q_matrix = np.zeros_like(r_matrix)
        q_matrix[np.isnan(r_matrix)] = np.nan

        return r_matrix, q_matrix

    def state(self):
        """
        Converts current position y,x=[0,0] to a linear state index to use in R, Q matrices
        :return: State linear index
        """
        return np.ravel_multi_index(self.position, self.board.shape)

    def reward(self):
        """
        Reward on the current position
        :return: Returns the reward value
        """
        return self.board[tuple(self.position)]

    def is_game_over(self):
        """
        Checks whether current field is marked as the end of game
        :return: Returns True if the game cannot continue
        """
        return self.game_over[tuple(self.position)]

    def move(self, action):
        """
        Moves agent in the specified direction
        :param action: Index of the action
        :return: Returns a copy object of the game with updated position
        :raises: Raises exception if the game is over or if the agent stepped out of the board
        """
        if self.is_game_over():
            raise Exception('Game Over')

        # update position by adding coordinates
        move = self.actions[action]
        new_position = self.position + move

        if (new_position >= 0).all() and (new_position < self.board.shape).all():  # checks boarders
            return self.__class__(
                current_position=new_position,
                board=self.board,
                actions=self.actions,
                game_over=self.game_over,
            )
        else:
            raise Exception('Stuck in the wall')


class FindTheCheeseGUI:
    width = 500
    height = 500

    def __init__(self, master, find_the_cheese):
        self.board = find_the_cheese.board
        self.position = find_the_cheese.position

        self.root = master
        self.canvas = tk.Canvas(self.root, width=self.width, height=self.height)
        self.canvas.pack()

        mouse_path = os.path.join(os.getcwd(), 'mouse.gif')
        cheese_path = os.path.join(os.getcwd(), 'cheese.gif')
        self.mouse_image = tk.PhotoImage(file=mouse_path) if os.path.isfile(mouse_path) else None
        self.cheese_image = tk.PhotoImage(file=cheese_path) if os.path.isfile(cheese_path) else None

        self.gw, self.gh = int(self.width / self.board.shape[0]), int(self.height / self.board.shape[1])
        self._grid()
        self._holes()
        self._cheese()
        self._mouse()

    def _grid(self):
        self.canvas.create_rectangle(0, 0, self.width, self.height, fill='white')
        for x in range(self.gw, self.width, self.gw):
            self.canvas.create_line(x, 0, x, self.height)
        for y in range(self.gh, self.height, self.gh):
            self.canvas.create_line(0, y, self.width, y)

    def _holes(self):
        holes = np.arange(self.board.size).reshape(self.board.shape)[self.board < 0]
        hole_loc = np.unravel_index(holes, self.board.shape)
        for y, x in zip(*hole_loc):
            x0, y0 = x * self.gw, y * self.gh
            x1, y1 = (x + 1) * self.gw, (y + 1) * self.gh
            self.canvas.create_rectangle(x0, y0, x1, y1, fill='black')

    def _cheese(self):
        cheese = np.arange(self.board.size).reshape(self.board.shape)[self.board > 0]
        ch_loc = np.unravel_index(cheese, self.board.shape)
        for y, x in zip(*ch_loc):
            x0, y0 = x * self.gw, y * self.gh
            x1, y1 = (x + 1) * self.gw, (y + 1) * self.gh
            self.canvas.create_rectangle(x0, y0, x1, y1, fill='yellow')

            if self.cheese_image is not None:
                self.cheese_img = self.canvas.create_image(
                    x0 + 10, y0 + 10,
                    image=self.cheese_image,
                    anchor=tk.NW,
                )

    def _mouse(self):
        y, x = self.position
        x0, y0 = x * self.gw, y * self.gh
        x1, y1 = (x + 1) * self.gw, (y + 1) * self.gh
        self.mouse_bg = self.canvas.create_rectangle(
            x0, y0,
            x1, y1,
            fill='lightgray',
        )
        if self.mouse_image is not None:
            self.mouse_head = self.canvas.create_image(
                x0 + 10, y0 + 10,
                image=self.mouse_image,
                anchor=tk.NW,
            )

    def move(self, position):
        y, x = self.position = position
        x0, y0 = x * self.gw, y * self.gh
        x1, y1 = (x + 1) * self.gw, (y + 1) * self.gh
        self.canvas.coords(self.mouse_bg, x0, y0, x1, y1)
        if self.mouse_image is not None:
            self.canvas.coords(self.mouse_head, x0 + 10, y0 + 10)


if __name__ == '__main__':
    # Initial board
    ftc = FindTheCheese(board=[
        [0, 0, 0, 0, 0],
        [0, -1, 0, 0, -1],
        [0, 0, 0, 0, 0],
        [0, 0, 0, 100, 0],
        [0, 0, 0, 0, 0],
    ])
    r, q = ftc.r_q_matrices()

    # init GUI
    root = tk.Tk()
    gui = FindTheCheeseGUI(root, ftc)
    root.update_idletasks()
    root.update()

    # Iteration
    episode_limit = 1000  # maximum episodes to compute
    stop_streak = 20  # stop iteration after this amount of successful games
    learn_factor = .1  # gamma learn factor
    frame_sleep = .1

    streak = 0  # streak counter
    it_meter = 0  # iteration counter

    for episode in range(episode_limit):
        _ftc = ftc  # get a copy reference to re-assign
        total_reward = 0.  # total reward in one game
        while not _ftc.is_game_over():
            it_meter += 1

            state = _ftc.state()
            q_state = q[state]  # Q matrix line

            # normalize the rewards in Q matrix
            # TODO beware the minimum is 0! - not possible to enter !!! good / bad?
            mini, maxi = np.nanmin(q_state), np.nanmax(q_state)
            # if there is zero difference between actions, spread evenly:
            q_normalized = (q_state - mini) / (maxi - mini) if maxi > mini else np.ones_like(q_state)
            q_normalized[np.isnan(q_state)] = 0  # borders to have zero probability

            # compute probability distribution for each action possible
            prob = q_normalized / np.sum(q_normalized)
            choice = np.random.choice(np.arange(len(q_normalized)), p=prob)

            # make move and capture updated game frame
            _ftc = _ftc.move(choice)
            total_reward += _ftc.reward()  # add up to total reward

            # recompute Q matrix values
            next_state = _ftc.state()
            q[state, choice] = r[state, choice] + learn_factor * np.nanmax(r[next_state])

            # refresh GUI
            gui.move(_ftc.position)
            root.update_idletasks()
            time.sleep(frame_sleep)

        # streak increment / reset
        streak = streak + 1 if total_reward > 0 else 0
        print(f'Episode#{episode} reward = {total_reward}; streak = {streak}')

        if streak >= stop_streak:
            print(f'Hit the stop streak {stop_streak}! stopping...')
            break

    print(f'Total iterations: {it_meter}')
