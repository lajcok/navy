"""
TEA - Time Escape Algorithm
"""
import tkinter as tk

import numpy as np
from matplotlib import pyplot as plt


def mandelbrot(c, m=2., iteration_limit=50):
    """
    Compute matrix according to Mandelbrot set,
    see https://en.wikipedia.org/wiki/Mandelbrot_set
    :param c: nd-array of complex numbers (coords) to compute set for
    :param m: Threshold `m`
    :param iteration_limit: Limit number of iterations
    :return: Returns nd-array made of `n` values for each coord in `c`
    """
    n = np.zeros_like(c, dtype=int)
    z = np.zeros_like(c, dtype=complex)
    todo = np.ones_like(c, dtype=bool)  # boolean mask of fields yet to iterate
    idx = np.arange(c.size).reshape(c.shape)    # index map helper
    for i in range(1, iteration_limit + 1):
        zi = z[todo] ** 2 + c[todo]     # compute only necessary
        valid = abs(zi) <= m            # check if more iterations needed
        if not valid.any():
            break
        idx_valid = np.unravel_index(idx[todo][valid], c.shape)
        idx_done = np.unravel_index(idx[todo][~valid], c.shape)
        n[idx_valid] = i            # update only fields <= m
        z[idx_valid] = zi[valid]
        todo[idx_done] = False      # update mask for which itearation stopped
    return n


def mandelbrot_functor(m=2., iteration_limit=50):
    """
    Mandelbrot functor to specify default configuration
    :param m:
    :param iteration_limit:
    :return:
    """
    def _mandelbrot(c, _m=m, _iter_limit=iteration_limit):
        return mandelbrot(c, _m, _iter_limit)

    return _mandelbrot


class Tea:
    """
    TEA Visualization
    """

    def __init__(self, resolution=200, rng=2., func=None):
        """
        Plot initialization
        :param resolution: (width, height) size tuple
        :param rng: Render range parameter, `m` to be passed to TEA function (if not specified explicitly)
        :param func: Function to be visualized
        """
        self.resolution = resolution
        self.range = rng
        self.mandelbrot = func if func is not None else mandelbrot_functor(m=rng)

        # positioning
        self.center = None
        self.scale = None

        # create figure
        self.fig = plt.figure()
        self.ax = self.fig.add_subplot(111)

        # bind events
        self.fig.canvas.mpl_connect('button_press_event', self.on_click)
        self.fig.canvas.mpl_connect('scroll_event', self.on_scroll)

    def on_click(self, event):
        """
        Zooms 2x to specified location
        """
        center = complex(event.xdata, event.ydata)
        scale = self.scale * 2
        self.render(center, scale)

    def on_scroll(self, event):
        """
        Scrolling zoom is more subtle, preserves center
        """
        scale = self.scale * 1.2 ** event.step
        self.render(self.center, scale)

    def render(self, center=0j, scale=1.):
        # store positioning
        self.center = center
        self.scale = scale

        # prepare positioned meshgrid view
        range_complex = self.range * (1 + 1j) / scale
        bottom_left, top_right = center - range_complex, center + range_complex
        c = np.sum(np.meshgrid(
            np.linspace(bottom_left.real, top_right.real, self.resolution),     # real part ~ x
            np.linspace(bottom_left - bottom_left.real, top_right - top_right.real, self.resolution), # imaginary part ~ y
        ), axis=0)

        # compute Mandelbrot set
        n = self.mandelbrot(c)

        # plot image
        extent = (bottom_left.real, top_right.real, bottom_left.imag, top_right.imag)
        self.ax.imshow(n, origin='lower', extent=extent)
        plt.show()


if __name__ == '__main__':
    tea = Tea(resolution=300)   # specify the detail
    tea.render()
