from enum import Enum

import numpy as np


class State(Enum):
    """
    Forest field states
    """
    Fire = -1
    Plain = 0
    Tree = 1

    def __int__(self):
        return self.value


def forest_fire(size, p, f):
    """
    Forest Fire Model iterative simulation
    :param size: Tuple `(width, height)` as size of the forest
    :param p: Probability that new tree grows
    :param f: Probability that random fire appears
    :return: Returns an iterator yielding forest states consisting of `State` values
    """
    # init forest area
    forest = np.full(size, fill_value=State.Plain.value, dtype=int)
    yield forest

    # iterate
    while True:
        # export state
        fires, plains, trees = [
            forest == state.value
            for state in State
        ]

        # spit fire
        fire_plan = np.zeros_like(forest, dtype=bool)

        # catching from neighbors
        fire_plan[:, 1:] |= fires[:, :-1]       # left
        fire_plan[:, :-1] |= fires[:, 1:]       # right
        fire_plan[1:] |= fires[:-1]             # top
        fire_plan[:-1] |= fires[1:]             # bottom
        fire_plan[1:, 1:] |= fires[:-1, :-1]    # top-left
        fire_plan[1:, :-1] |= fires[:-1, 1:]    # top-right
        fire_plan[:-1, 1:] |= fires[1:, :-1]    # bottom-left
        fire_plan[:-1, :-1] |= fires[1:, 1:]    # bottom-right

        fire_plan[...] &= trees                 # only apply on trees
        trees[fire_plan] = 0                    # already burning, right?
        fire_plan[trees] = np.random.random_sample(trees.sum()) < f     # dry season

        # update state
        forest[fires] = State.Plain
        forest[fire_plan] = State.Fire

        # grow random trees
        forest[plains] = (np.random.random_sample(plains.sum()) < p) * State.Tree.value

        yield forest


def main():
    from matplotlib import pyplot as plt, colors as clr, animation as ani, patches as ptc

    # init simulation
    ff = forest_fire(size=[200] * 2, p=.03, f=.003)

    # visualization
    fig = plt.figure('Forest Fire Model')
    ax = fig.add_subplot(111)

    # custom color map
    color_levels = ['orange', 'brown', 'green']
    patches = [
        ptc.Patch(color=c, label=s.name)
        for c, s in zip(color_levels, State)
    ]
    color_map = clr.LinearSegmentedColormap.from_list('Forest Fire', color_levels, len(color_levels))

    # initial area, animation
    im = ax.imshow(
        next(ff),
        vmin=min([s.value for s in State]),
        vmax=max([s.value for s in State]),
        cmap=color_map,
    )
    fig.legend(handles=patches)

    def update(frame):
        _, im_data = frame
        im.set_array(im_data)
        return im,

    _ = ani.FuncAnimation(
        fig, update,
        frames=enumerate(ff),
        interval=20,
        blit=True,
    )

    plt.show()


if __name__ == '__main__':
    main()
