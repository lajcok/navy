import numpy as np


class Perceptron:
    def __init__(self, fn=np.sign, ws=(1.,), b=0., epsilon=.1):
        self.fn = fn
        self.ws = np.array(ws, dtype=float)
        self.b = b
        self.epsilon = epsilon

    def guess(self, xs):
        return self.fn(self.ws.dot(xs) + self.b)

    def teach(self, xs, error):
        n_ws = self.ws + xs * error * self.epsilon
        return self.__class__(self.fn, ws=n_ws, b=self.b, epsilon=self.epsilon)
