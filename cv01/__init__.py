import numpy as np
from matplotlib import pyplot as plt

from cv01.perceptron import Perceptron


def fn_check(fn):
    def _error(xs, y_guess):
        y = fn(xs)
        return y - y_guess

    return _error


if __name__ == '__main__':
    fn = np.vectorize(lambda x: 4 * x - 5)
    def position(xs): return np.sign(xs[1] - fn(xs[0]))
    fnc = fn_check(position)

    n, dim = 1000, 2
    low, high = (-100, -150), (100, 50)
    pts = np.random.uniform(low, high, (n, dim))

    epochs = 10
    p = Perceptron(ws=(1, 1))
    for i in range(epochs):
        guesses = [p.guess(xs) for xs in pts]
        errors = [fnc(xs, y_guess) for xs, y_guess in zip(pts, guesses)]
        hits = len(list(filter(lambda e: e == 0, errors)))
        print(i, hits, p.ws)

        for xs, error in zip(pts, errors):
            p = p.teach(xs, error)

    x_lim, y_lim = tuple([
        [low[di], high[di]]
        for di in range(dim)
    ])
    plt.xlim(x_lim)
    plt.ylim(y_lim)

    positions = np.apply_along_axis(position, axis=1, arr=pts)
    below, on, above = positions < 0, positions == 0, positions > 0
    size = 10
    plt.scatter(x=pts[below, 0], y=pts[below, 1], marker='v', c='coral', s=size)
    plt.scatter(x=pts[above, 0], y=pts[above, 1], marker='o', c='violet', s=size)
    plt.plot(x_lim, fn(x_lim), 'r')

    approx_fn = np.vectorize(lambda x: - p.ws[0] / p.ws[1] * x)
    plt.plot(x_lim, approx_fn(x_lim), 'g--')

    plt.scatter(x=pts[on, 0], y=pts[on, 1], marker='X', c='blue')

    plt.show()
